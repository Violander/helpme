//
//  SignUpViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    let TagName = 0
    let TagSurname = 1
    let TagEmail = 2
    let TagPassword = 3
    
    var name: String?
    var surname: String?
    var email: String?
    var password: String?
    
    var loadingView: UIView!
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var constraintTopStackViewTExtFields: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Create a loading view
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        //Check if user is already logged and push to home screen
        if Auth.auth().currentUser != nil {
            AppDelegate.instance.currentUserUid = Auth.auth().currentUser?.uid
            AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let user = snapshot.value as? NSDictionary {
                    AppDelegate.instance.currentUserName = user["name"] as! String
                    AppDelegate.instance.currentUserSurname = user["surname"] as! String
                    AppDelegate.instance.currentUserIdNumber = user["idNumber"] as! String
                }
                self.loadingView.isHidden = true
                self.performSegue(withIdentifier: Constants.Segues.GoToHome, sender: nil)
            })
        } else {
            self.loadingView.isHidden = true
        }
        //Add observer to keyboard to adjust textfields
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSignUpButtonTap() {
        //Stop Editing
        self.view.endEditing(true)
        //Start show loading indicator
        self.loadingView.isHidden = false
        //If Fields are not empty
        if self.validateItems() {
            //Signup User
            Auth.auth().createUser(withEmail: self.email!, password: self.password!) { (user, error) in
                if user != nil {
                    AppDelegate.instance.currentUserUid = user!.uid
                    AppDelegate.instance.ref.child("users/\(user!.uid)/name").setValue(self.name!)
                    AppDelegate.instance.ref.child("users/\(user!.uid)/surname").setValue(self.surname!)
                    AppDelegate.instance.ref.child("users/\(user!.uid)/type").setValue("patient")
                    AppDelegate.instance.ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        // Get user value
                        let value = snapshot.value as? NSDictionary
                        
                        let idNumber = "0\(value?.count ?? 0)"
                        AppDelegate.instance.ref.child("users/\(user!.uid)/idNumber").setValue(idNumber)
                        
                        AppDelegate.instance.ref.child("patients/members/\(idNumber)").setValue(user!.uid)
                        AppDelegate.instance.ref.child("users/\(user!.uid)/surname").setValue(self.surname!)
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                        }
                        self.performSegue(withIdentifier: Constants.Segues.GoToHome, sender: nil)
                    }) { (error) in
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            let _ = SweetAlert().showAlert("", subTitle: error.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
                        }
                    }
                } else if error != nil {
                    
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        let _ = SweetAlert().showAlert("", subTitle: error!.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        let _ = SweetAlert().showAlert("", subTitle: "Failed To Sign In", style: .error, buttonTitle: "Ok", action: nil)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                let _ = SweetAlert().showAlert("", subTitle: "Empty Fields", style: .error, buttonTitle: "Ok", action: nil)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case TagName:
            self.name = textField.text
            break
        case TagSurname:
            self.surname = textField.text
            break
        case TagEmail:
            self.email = textField.text
            break
        case TagPassword:
            self.password = textField.text
            break
        default:
            break
        }
    }
    
    func validateItems() -> Bool {
        if self.name == nil || self.name == "" || self.surname == nil || self.surname == "" || self.email == nil || self.email == "" || self.password == nil || self.password == ""{
            return false
        }
        return true
    }
    
    
    //MARK: - Notifications
    @objc
    func keyboardWillShow(notification: NSNotification) {
        if self.constraintTopStackViewTExtFields.constant == 0 {
            self.constraintTopStackViewTExtFields.constant -= 90
            self.view.needsUpdateConstraints()
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        if self.constraintTopStackViewTExtFields.constant != 0{
            self.constraintTopStackViewTExtFields.constant = 0
            self.view.needsUpdateConstraints()
        }
    }
}
