//
//  NotificationsArchiveTableViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class NotificationsArchiveTableViewController: UITableViewController {
    
    var notifications: [String]!
    var loadingView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.notifications = []
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.tableView.allowsSelection = false
        //Get User notifications
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("notifications").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? [String: AnyObject]
            if value != nil {
                self.notifications = Array(value!.keys)
            }
            
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.tableView.reloadData()
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Show notification in table view cell
        let notification = self.notifications[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.NotificationCell, for: indexPath) as! NotificationTableViewCell
        cell.labelNumber.text = "Notification: \(indexPath.row+1)"
        cell.labelDate.text = "Sent: \(notification)"

        return cell
    }
}
