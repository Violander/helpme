//
//  HelpMeViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class HelpMeViewController: UIViewController {
    
    @IBOutlet weak var buttonHelpMe: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.instance.specialTabBarController?.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onButtonHelpMeTap() {
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("linked").observeSingleEvent(of: .value, with: { (snapshot) in
            //Check for caregivers
            if let caregivers = snapshot.value as? NSDictionary {
                if caregivers.count > 0 {
                    //Send notification to my topic
                    let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
                    var request = URLRequest(url: url)
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("key=\(Constants.Notifications.ServerKey)", forHTTPHeaderField: "Authorization")
                    request.httpMethod = "POST"
                    let notification = ["body": "\(AppDelegate.instance.currentUserName + " " + AppDelegate.instance.currentUserSurname) needs your help", "title":"HELP ME PLEASE!!!"]
                    let parameters = ["to":"/topics/\(AppDelegate.instance.currentUserIdNumber!)", "priority": "high", "notification": notification] as [String : Any]
                    let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
                    request.httpBody = httpBody
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        if error != nil {
                            DispatchQueue.main.async {
                                let _ = SweetAlert().showAlert(error!.localizedDescription, subTitle: "", style: .error, buttonTitle: "Ok", action: nil)
                            }
                        } else if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                            DispatchQueue.main.async {
                                let _ = SweetAlert().showAlert("Bad statusCode:  \(httpStatus.statusCode)", subTitle: "", style: .error, buttonTitle: "Ok", action: nil)
                            }
                        }
                        DispatchQueue.main.async {
                            let _ = SweetAlert().showAlert("Request correctly sent", subTitle: "", style: .success, buttonTitle: "Ok", action:{ (success) in
                                let date = Date()
                                AppDelegate.instance.ref.child("users/\(AppDelegate.instance.currentUserUid!)/notifications/\(date.toString(dateFormat: "yyyy-MM-dd HH:mm:ss"))").setValue(true)
                            })
                        }
                    }
                    task.resume()
                } else {
                    DispatchQueue.main.async {
                        let _ = SweetAlert().showAlert("No caregivers found", subTitle: "", style: .error, buttonTitle: "Ok", action: nil)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    let _ = SweetAlert().showAlert("No caregivers found", subTitle: "", style: .error, buttonTitle: "Ok", action: nil)
                }
            }
        }) { (error) in
            DispatchQueue.main.async {
                let _ = SweetAlert().showAlert("", subTitle: error.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
            }
        }
    }
    
    func initializeLayout() {
        self.buttonHelpMe.layer.borderWidth = 5.0
        self.buttonHelpMe.layer.borderColor = self.buttonHelpMe.titleColor(for: UIControlState.normal)?.cgColor
        self.buttonHelpMe.layer.cornerRadius = 30
        self.buttonHelpMe.layer.masksToBounds = true
    }
    
}
