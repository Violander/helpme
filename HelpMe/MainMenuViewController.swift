//
//  MainMenuViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import FirebaseAuth

class MainMenuViewController: UIViewController {
    @IBOutlet weak var viewProfileIcon: UIView!
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var buttonNotifications: UIButton!
    
    @IBOutlet weak var labelIDNumber: UILabel!
    @IBOutlet weak var labelNameSurname: UILabel!
    
    var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let name = value?["name"] as? String ?? ""
            let surname = value?["surname"] as? String ?? ""
            let idNumber = value?["idNumber"] as? String ?? ""
            //Update UI
            DispatchQueue.main.async {
                self.labelIDNumber.text = idNumber
                self.labelNameSurname.text = name+" "+surname
                self.loadingView.isHidden = true
            }
        }) { (error) in
            DispatchQueue.main.async {
                let _ = SweetAlert().showAlert("", subTitle: error.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
            }
        }
        
        self.initiailizeLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Configure Layout
    func initiailizeLayout() {
        self.viewProfileIcon.layer.cornerRadius = self.viewProfileIcon.frame.width / 3
        self.viewProfileIcon.layer.masksToBounds = true
        
        let lineViewTop = UIView(frame: CGRect(x: 0, y: 0, width: self.buttonLogout.frame.size.width, height: 1.0))
        lineViewTop.backgroundColor=UIColor.white.withAlphaComponent(0.5)
        self.buttonLogout.addSubview(lineViewTop)
        
        let lineViewBottom = UIView(frame: CGRect(x: 0, y: self.buttonLogout.frame.size.height - 1.0, width: self.buttonLogout.frame.size.width, height: 1.0))
        lineViewBottom.backgroundColor=UIColor.white.withAlphaComponent(0.5)
        self.buttonLogout.addSubview(lineViewBottom)
    }
    
    
    @IBAction func onNotificationButtonArchiveTap() {
        self.performSegue(withIdentifier: Constants.Segues.GoToNotificationArchive, sender: nil)
    }
    
    @IBAction func onLogoutButtonTapped() {
        try! Auth.auth().signOut()
        self.dismiss(animated: true) {
            AppDelegate.instance.specialTabBarController?.navigationController?.popToRootViewController(animated: false)
        }
    }
}
