//
//  CaregiversTableViewCell.swift
//  HelpMe
//
//  Created by Romeo Violini on 22/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class CaregiversTableViewCell: UITableViewCell {

    @IBOutlet weak var labelDisplayName: UILabel!
    @IBOutlet weak var labelIDNumber: UILabel!
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
