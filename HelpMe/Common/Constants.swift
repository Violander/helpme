//
//  Constants.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Notifications {
        static let ServerKey = "AAAAWLr-o6g:APA91bHpLklVdDI62joCgt4bZ6_z8_cq31vhhIlPzGQm9TSSrZu3aoegV5XOYfM5G89phk-HfKC4uFd_UFyghgLbCg61-MYNcVvzIoEubG-xE9c43UYms88S-vveUd5MoLW1uOyvIxWR"
    }
 
    struct Segues {
        static let GoToSignIn = "goToSignIn"
        static let GoToHome = "goToHome"
        static let ShowMainMenu = "showMainMenu"
        static let GoToNotificationArchive = "goToNotificationArchive"
        static let OpenPopupAddCaregiver = "openPopupAddCaregiver"
    }
    
    struct Cell {
        static let CareGiversCell = "CareGiversCell"
        static let NotificationCell = "notificationCell"
    }
    
}
