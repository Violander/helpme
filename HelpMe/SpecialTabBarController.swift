//
//  SpecialTabBarController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import SideMenu

protocol SpecialTabBarControllerPopupDelegate: class {
    
    func specialTabBarControllerPopupShouldDismiss(popoverPresentationController: UIPopoverPresentationController) -> Bool
    func specialTabBarControllerPopupDidDismiss(popoverPresentationController: UIPopoverPresentationController)
}

class SpecialTabBarController: UITabBarController, UIPopoverPresentationControllerDelegate {
    
    weak var specialBarDelegate : SpecialTabBarControllerPopupDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppDelegate.instance.specialTabBarController = self
        
        let navController = self.navigationController as! SpecialNavigationController
        navController.setCurrentStatusBarStyle(statusBarStyle: UIStatusBarStyle.lightContent)
        
        self.navigationItem.leftBarButtonItem?.target = self
        self.navigationItem.leftBarButtonItem?.action = #selector(openMenu)
        
        
        let menuController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainMenu") as! UISideMenuNavigationController
        
        SideMenuManager.default.menuLeftNavigationController = menuController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu: UIRectEdge.left)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc
    func openMenu() {
        self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.OpenPopupAddCaregiver {
            segue.destination.popoverPresentationController!.delegate = self
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return (self.specialBarDelegate?.specialTabBarControllerPopupShouldDismiss(popoverPresentationController: popoverPresentationController)) ?? true
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        self.specialBarDelegate?.specialTabBarControllerPopupDidDismiss(popoverPresentationController: popoverPresentationController)
    }
}
