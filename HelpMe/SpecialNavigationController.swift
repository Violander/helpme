//
//  SpecialNavigationController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class SpecialNavigationController: UINavigationController {
    
    var currentStatusBarStyle : UIStatusBarStyle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if self.currentStatusBarStyle != nil {
            return self.currentStatusBarStyle!
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    
    func setCurrentStatusBarStyle (statusBarStyle : UIStatusBarStyle) {
        
        self.currentStatusBarStyle = statusBarStyle
        self.setNeedsStatusBarAppearanceUpdate()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
