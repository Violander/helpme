//
//  MySideMenuNavigationController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import SideMenu

class MySideMenuNavigationController: UISideMenuNavigationController {

    let customSideMenuManager = SideMenuManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sideMenuManager = customSideMenuManager
    }
}
