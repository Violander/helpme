//
//  SignInViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignInViewController: UIViewController, UITextFieldDelegate {
    
    let TagEmail = 0
    let TagPassword = 1
    
    var email: String?
    var password: String?
    
    var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSignInButtonTap() {
        //Stop editing
        self.view.endEditing(true)
        //Show loading indicator
        self.loadingView.isHidden = false
        //Check if fields are not empty
        if self.validateItems() {
            //Signin user
            Auth.auth().signIn(withEmail: self.email!, password: self.password!) { (user, error) in
                self.loadingView.isHidden = true
                if user != nil {
                    AppDelegate.instance.currentUserUid = user!.uid
                    AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
                        if let user = snapshot.value as? NSDictionary {
                            AppDelegate.instance.currentUserName = user["name"] as! String
                            AppDelegate.instance.currentUserSurname = user["surname"] as! String
                            AppDelegate.instance.currentUserIdNumber = user["idNumber"] as! String
                        }
                        self.performSegue(withIdentifier: Constants.Segues.GoToHome, sender: nil)
                    })
                } else if error != nil {
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        let _ = SweetAlert().showAlert("", subTitle: error!.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        let _ = SweetAlert().showAlert("", subTitle: "Failed To Sign In", style: .error, buttonTitle: "Ok", action: nil)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                let _ = SweetAlert().showAlert("", subTitle: "Empty Fields", style: .error, buttonTitle: "Ok", action: nil)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case TagEmail:
            self.email = textField.text
            break
        case TagPassword:
            self.password = textField.text
            break
        default:
            break
        }
    }
    
    func validateItems() -> Bool {
        if self.email == nil || self.email == "" || self.password == nil || self.password == ""{
            return false
        }
        return true
    }
}
