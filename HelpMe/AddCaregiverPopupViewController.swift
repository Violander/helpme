//
//  AddCaregiverPopupViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 22/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class AddCaregiverPopupViewController: UIViewController, UITextFieldDelegate {
    
    var careGiverIdNumber: String?
    
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancelTap() {
        self.popoverPresentationController!.delegate?.popoverPresentationControllerDidDismissPopover!(self.popoverPresentationController!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAddTap() {
        //Stop Editing
        self.view.endEditing(true)
        //Check for Empty Fields
        if self.validateItem() {
            
            AppDelegate.instance.ref.child("caregivers").child("members").child(self.careGiverIdNumber!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let uid = snapshot.value as? String {
                    //Check if is already your caregiver
                    AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("linked").child(self.careGiverIdNumber!).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if let _ = snapshot.value as? String {
                            DispatchQueue.main.async {
                                let _ = SweetAlert().showAlert("Already your caregiver", subTitle: "", style: .warning, buttonTitle: "Ok", action: nil)
                            }
                        } else {
                            //Check if is already in your pending list
                            AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("pending").child(self.careGiverIdNumber!).observeSingleEvent(of: .value, with: { (snapshot) in
                                if let _ = snapshot.value as? String {
                                    DispatchQueue.main.async {
                                        let _ = SweetAlert().showAlert("Already in your Pending list", subTitle: "", style: .warning, buttonTitle: "Ok", action: nil)
                                    }
                                } else {
                                    //Send request to user
                                    AppDelegate.instance.ref.child("users/\(AppDelegate.instance.currentUserUid!)/pending/\(self.careGiverIdNumber!)").setValue(uid)
                                    AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
                                        
                                        let value = snapshot.value as! [String: AnyObject]
                                        let myIdNumber = value["idNumber"] as! String
                                        AppDelegate.instance.ref.child("users/\(uid)/pending/\(myIdNumber)").setValue(AppDelegate.instance.currentUserUid!)
                                    })
                                }
                            })
                        }
                    })
                } else {
                    DispatchQueue.main.async {
                        let _ = SweetAlert().showAlert("No caregivers found", subTitle: "", style: .error, buttonTitle: "Ok", action: nil)
                    }
                }
            })
        } else {
            DispatchQueue.main.async {
                let _ = SweetAlert().showAlert("id number not valid", subTitle: "", style: .error, buttonTitle: "Ok", action: nil)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.careGiverIdNumber = textField.text
    }
    
    func validateItem() -> Bool {
        
        if self.careGiverIdNumber == nil || self.careGiverIdNumber == "" {
            return false
        }
        return true
    }
}
