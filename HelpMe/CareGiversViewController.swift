//
//  CareGiversViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class CareGiversViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, SpecialTabBarControllerPopupDelegate {
  
    let tagLinked = 0
    let tagPending = 1
    
    var caregiversLinked: [String: String]!
    var caregiversPending: [String: String]!
    
    var loadingView: UIView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.instance.specialTabBarController?.specialBarDelegate = self
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.caregiversLinked = [:]
        self.caregiversPending = [:]
        self.tableView.allowsSelection = false
        self.loadingView.isHidden = false
    
       //Get Caregivers Linked and Pending
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? [String: AnyObject]
            self.caregiversLinked = value?["linked"] as? [String: String] ?? [:]
            self.caregiversPending = value?["pending"] as? [String: String] ?? [:]
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.tableView.reloadData()
            }
        })
       
         //Observing Linked and Pending caregivers
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("linked").observe(.childAdded) { (snapshot) in
            //let dic =  [snapshot.key: snapshot.value as! String]
            self.caregiversLinked[snapshot.key] = snapshot.value as? String
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("pending").observe(.childAdded) { (snapshot) in
            self.caregiversPending[snapshot.key] = snapshot.value as? String
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("pending").observe(.childRemoved) { (snapshot) in
            let key = snapshot.key
            self.caregiversPending.removeValue(forKey: key)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        //Initialize Layout
        self.initializeLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.instance.specialTabBarController?.navigationItem.rightBarButtonItem?.isEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeLayout() {
        self.segmentedControl.removeBorders()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentedControl.selectedSegmentIndex == tagLinked {
            return self.caregiversLinked.count
        } else {
            return self.caregiversPending.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Display Caregiver Pending or Linked according to segment control
        var careGiverUUID = ""
        if self.segmentedControl.selectedSegmentIndex == tagLinked {
            careGiverUUID = self.caregiversLinked[Array(self.caregiversLinked.keys)[indexPath.row]]!
        } else {
            careGiverUUID = self.caregiversPending[Array(self.caregiversPending.keys)[indexPath.row]]!
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.CareGiversCell, for: indexPath) as! CaregiversTableViewCell
        cell.loadingIndicator.isHidden = false
        cell.labelIDNumber.isHidden = true
        cell.labelDisplayName.isHidden = true
        print(careGiverUUID)
        AppDelegate.instance.ref.child("users").child(careGiverUUID).observeSingleEvent(of: .value, with: { (snapshot) in
            let caregiver = snapshot.value as? [String: AnyObject]
            DispatchQueue.main.async {
                cell.labelDisplayName.text = (caregiver!["name"] as! String) + " " + (caregiver!["surname"] as! String)
                cell.labelIDNumber.text = "IDNumber: " + (caregiver!["idNumber"] as! String)
                cell.labelIDNumber.isHidden = false
                cell.labelDisplayName.isHidden = false
                cell.loadingIndicator.isHidden = true
                cell.loadingIndicator.isHidden = true
            }
        })
        return cell
    }
    
    @IBAction func onSegmnentedControlChangeValue() {
        self.tableView.reloadData()
    }
    
    func specialTabBarControllerPopupShouldDismiss(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    func specialTabBarControllerPopupDidDismiss(popoverPresentationController: UIPopoverPresentationController) {
    }
}
